package com.hproject.hibernate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hproject.hibernate.model.Note;

@Repository
public interface NoteRepository extends JpaRepository<Note, Long> {

}
